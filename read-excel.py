import openpyxl

book = openpyxl.load_workbook('filename.xlsx')

sheet = book.active

for row in range(2,104+1):
    tc = sheet.cell(row=row, column=1).value
    priority = sheet.cell(row=row, column=2).value
    category = sheet.cell(row=row, column=3).value
    synopsis = sheet.cell(row=row, column=6).value
    procedure = sheet.cell(row=row, column=7).value
    verification = sheet.cell(row=row, column=8).value
    procedure_lines = ""
    for line in procedure.splitlines():
        procedure_lines += "\n|    | ... | *                      * " + line + " \\n"
    if verification:
        verification_lines = ""
        for line in verification.splitlines():
            verification_lines += "\n|    | ... | *                      * " + line + " \\n"
    else:
        verification_lines = "None"
